# EPSI-ML

## Problèmatique

Prédire la Synthese_eval_sanit en fonction de la date de création de l'entreprise, du jour et du mois, de la densité de population

## Consolidation des données

Nous avons rajouté aux données de base des données apportant plus de contexte sur les établissements comme : 
- La date de création récupérée à partir du SIRET
- La population et superficie de la commune à partir du code postal. On peut ensuite en déduire la densité de population au km²

## Nos modèles

RandomForest
Régréssion Logistique

## Démarche

**Pour le modèle de Régression Logistique :**
Nous avons commencé par prétraiter les données en utilisant des encodeurs de labels pour les colonnes catégorielles 'filtre' et 'activite'. Cela nous a permis de convertir ces caractéristiques en valeurs numériques exploitables par le modèle. Ensuite, nous avons divisé les données en ensembles d'entraînement et de test, avec 80% des données pour l'entraînement et 20% pour les tests. Nous avons utilisé un modèle de Régression Logistique avec un nombre d'itérations maximal de 1000 pour l'entraînement. Après l'entraînement, nous avons évalué le modèle en utilisant l'ensemble de test et calculé la précision du modèle, qui était d'environ 53,72%.

**Pour le modèle Random Forest :**
Nous avons également effectué un prétraitement similaire, mais cette fois, nous avons utilisé une approche différente pour encoder les caractéristiques catégorielles. Ensuite, nous avons divisé les données en ensembles d'entraînement et de test, avec la même répartition que pour la Régression Logistique. Nous avons utilisé un modèle de RandomForestClassifier pour l'entraînement. Initialement, le modèle Random Forest a obtenu une précision d'environ 80%, ce qui était significativement plus élevé que la Régression Logistique. Ensuite, nous avons effectué une analyse en retirant séquentiellement chaque colonne des caractéristiques pour évaluer l'impact sur les performances du modèle.

## Résumé

Après avoir testé les modèles **Random Forest** et **Régression Logistique** pour prédire l'évaluation des restaurants à partir de nos données, nous avons obtenu des résultats différents. Le modèle **Random Forest** a produit un score d'environ **0.8**, ce qui indique une précision élevée. En revanche, le modèle de **Régression Logistique** a obtenu un score d'environ **0.5**, révélant une précision plus faible. 

En résumé, nous avons prétraité les données de manière similaire pour les deux modèles, mais le modèle Random Forest a montré une précision beaucoup plus élevée par rapport au modèle de Régression Logistique. Nous avons également effectué une analyse pour comprendre l'importance de chaque colonne dans le modèle Random Forest.

Ces résultats nous montre que le modèle **Random Forest** est mieux adapté à notre problème.

## Membres projet

Pierre BEC
Lucas CASSAN
Tristan MULLER

## Prédictions

Différentes prédictions ont été effectuées afin de tester les modèles entrainés.

### Forêt aléatoire

Pour ce modèle, **100 prédictions** ont été réalisées en tirant **aléatoirement** des modalités pour chaque variable de classe. Ce résultat stochastique représente donc approximativement les résultats obtenus dans un cas concrêt ([en savoir plus...](https://gitlab.com/Lucas-Cassan/epsi-ml/-/blob/1f8f2645bbf2a67613866ab2ff30ac3be46c898a/random_forest/random_forest.ipynb)).

Profilage à partir de ces prédictions :

![Diagramme  de profilage des prédictions](predictions_random_forest.png)