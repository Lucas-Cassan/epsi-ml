#!/bin/bash

# Nom du fichier CSV d'entrée
fichier_entree="StockEtablissement_utf8.csv"

# Nom du fichier CSV de sortie
fichier_sortie="export_StockEtablissement_utf8.csv"

# Utilisez cut pour extraire les colonnes spécifiées et les mettre dans des fichiers temporaires
cut -d ',' -f 3 "$fichier_entree" > temp_siret.csv
cut -d ',' -f 5 "$fichier_entree" > temp_date.csv

# Utilisez paste pour fusionner les deux colonnes extraites en un nouveau fichier CSV
paste -d ',' temp_siret.csv temp_date.csv > "$fichier_sortie"

# Supprimez les fichiers temporaires
rm temp_siret.csv temp_date.csv

echo "Colonnes extraites avec succès et enregistrées dans $fichier_sortie"